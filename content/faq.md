---
title: "FAQs"
description: "Frequently Asked Questions"
date: "2018-02-11"
---

## Quick Links
+ [MSU Ballroom Dance Team Constitution](https://drive.google.com/file/d/1kdS0TsTv3MCXypdS7Ja4N0d4NmS4bWAR/view?usp=sharing)
+ [MSU Ballroom Dance Team Meeting Notes](https://drive.google.com/drive/folders/0B01XFsIqZCCHRWNReWRJdWx5X0E?usp=sharing)

## I have no experience with ballroom, general dancing, or even moving my legs

No experience necessary! Only 1-2 of our new members **yearly** actually have dance experience in any style.

## I have two left feet!

Brendan, our secretary (who is also writing this in the third person), would retort with that he had 3 left feet and now he has one left, one right and knows how to dance!

## Dancing is for losers!

If losers are the life of the party, have tons of friends, are invited to events almost weekly, find significant others, win ribbons at competitions, and have cool apparel... Then yes. Yes we are.

## So I have to compete? I don't feel comfortable!
You do **NOT** have to compete. It's completely optional, however, we provide the opportunity and the resources for those who do.

